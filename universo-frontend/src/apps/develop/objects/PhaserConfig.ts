import { AUTO, Scale, Types } from 'phaser';

import SpinnerPlugin from 'phaser3-rex-plugins/templates/spinner/spinner-plugin.js';
import LoadingAnimationScenePlugin from 'phaser3-rex-plugins/plugins/loadinganimationscene-plugin.js';

export const getPhaserConfig = (scene: any[]): Types.Core.GameConfig => ({
  type: AUTO,
  parent: 'phaser-container',
  width: window.innerWidth,
  height: window.innerHeight,
  scale: {
    mode: Scale.FIT,
    autoCenter: Scale.CENTER_BOTH,
  },
  scene: scene,
  plugins: {
    // scene: [
    // {
    // key: 'rexUI',
    // plugin: RexPlugins,
    // mapping: 'rexUI',
    // },
    // // ...
    // ],
    global: [
      {
        key: 'rexLoadingAnimationScene',
        plugin: LoadingAnimationScenePlugin,
        start: true,
      },
    ],
    scene: [
      {
        key: 'rexSpinner',
        plugin: SpinnerPlugin,
        mapping: 'rexSpinner',
      },
    ],
  },
});
