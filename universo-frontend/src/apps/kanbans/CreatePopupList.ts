import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateTextObject from './CreateTextObject';
import CreateRequestModalColumn from './CreateRequestModalColumn';
import CreateRequestModalBoard from './CreateRequestModalBoard';
import { EndlessCanvas } from 'src/types/kanban_types';
import ObjectWrapper from 'src/utils/objectWrapper';

const CreatePopupList = function (
  scene: EndlessCanvas,
  board: ObjectWrapper,
  x: number,
  y: number,
  options: string[],
  onClick: (button: Phaser.GameObjects.Text) => void,
) {
  let textInput = '';
  const items = options.map((option) => ({ label: option }));
  const menu = scene.rexUI.add.menu({
    x: x,
    y: y,
    orientation: 'y',
    items: items,
    createButtonCallback: (item, i, options) => {
      return scene.rexUI.add.label({
        background: scene.rexUI.add.roundRectangle(0, 0, 2, 2, 0, COLOR_DARK),
        text: CreateTextObject(scene, item.label),
        space: {
          left: 10,
          right: 10,
          top: 10,
          bottom: 10,
          icon: 10,
        },
      });
    },
    easeIn: {
      duration: 500,
      orientation: 'y',
    },
    easeOut: {
      duration: 100,
      orientation: 'y',
    },
  });

  menu.on('button.click', function (button: Phaser.GameObjects.Text) {
    if (button.text === 'Добавить столбец') {
      CreateRequestModalColumn(scene)
        .setPosition(400, 300)
        .layout()
        .modalPromise({
          manualClose: true,
          duration: {
            in: 500,
            out: 500,
          },
        })
        .then(function (data) {
          console.log(data);
        });
    } else if (button.text === 'Переименовать доску') {
      CreateRequestModalBoard(scene, board, function (text) {
        console.log(`Content: '${text}'`);
        textInput = text;
      })
        .setPosition(400, 300)
        .layout()
        .modalPromise({
          manualClose: true,
          duration: {
            in: 500,
            out: 500,
          },
        })
        .then(function (data) {
          //@ts-ignore
          if (data.text === 'Сохранить') {
            if (textInput) {
              const payload = {
                nomo: textInput,
                uuid: board.uuid,
                //@ts-ignore
                // kanvasoUuid: scene.store.getKanvaso[0].node.uuid,
              };
              //@ts-ignore
              scene.store.onEditKanvasoObjekto(payload);
            }
          }
        });
    }
    menu.collapse();
  });

  return menu;
};
export default CreatePopupList;
