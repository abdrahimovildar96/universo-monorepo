import { EndlessCanvas } from 'src/types/kanban_types';
import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateModalLabel from './CreateModalLabel';
import ObjectWrapper from 'src/utils/objectWrapper';

const CreateRequestModalBoard = (
  scene: EndlessCanvas,
  board: ObjectWrapper,
  callback,
) => {
  //@ts-ignore
  const content = scene.store.getCurrentState.get(board.uuid).name;

  const textArea = scene.rexUI.add
    .textAreaInput({
      x: 400,
      y: 300,
      width: 200, // Увеличена ширина
      height: 20,

      background: scene.rexUI.add.roundRectangle(
        0,
        0,
        20,
        20,
        0,
        COLOR_PRIMARY,
      ),

      text: {
        background: {
          stroke: 'black',
        },

        style: {
          fontSize: 20,
          //@ts-ignore

          backgroundBottomY: 1,
          backgroundHeight: 20,
          cursor: {
            color: 'black',
            backgroundColor: 'white',
          },
          color: '#ffffff',
        },
      },

      space: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        text: 10,
        header: 0,
        footer: 0,
      },

      mouseWheelScroller: {
        focus: false,
        speed: 0.1,
      },

      content: content,
    })
    .layout()
    .on('textchange', function (text: string) {
      //console.log(`Content: '${text}'`);
      callback(text);
    });

  textArea.setDepth(2);
  const cancelButton = scene.rexUI.add
    .label({
      background: scene.rexUI.add.roundRectangle(0, 0, 0, 0, 20, COLOR_DARK),
      text: scene.add.text(0, 0, 'Отменить', { fontSize: '24px' }),
      space: { left: 10, right: 10, top: 10, bottom: 10 },
    })
    .setDepth(3);

  const saveButton = scene.rexUI.add
    .label({
      background: scene.rexUI.add.roundRectangle(0, 0, 0, 0, 20, COLOR_LIGHT),
      text: scene.add.text(0, 0, 'Сохранить', { fontSize: '24px' }),
      space: { left: 10, right: 10, top: 10, bottom: 10 },
    })

    .setDepth(3);
  const dialog = scene.rexUI.add
    .dialog({
      background: scene.rexUI.add
        .roundRectangle(0, 0, 1020, 100, 20, 0xffffff) // Увеличена ширина
        .setStrokeStyle(2, 0x000000),

      title: scene.rexUI.add.label({
        text: scene.add.text(0, 0, 'Редактирование объекта', {
          fontSize: '24px',
          color: '#000000',
        }),
        space: {
          left: 20, // Увеличены отступы
          right: 20,
          top: 15,
          bottom: 15,
        },
      }),

      content: textArea,

      actions: [cancelButton, saveButton],

      space: {
        title: 30, // Увеличены отступы
        content: 30,
        action: 20,

        left: 25,
        right: 25,
        top: 25,
        bottom: 25,
      },

      align: {
        actions: 'right',
      },

      expand: {
        content: false,
      },
    })
    .on('button.over', function (button, groupName, index, pointer, event) {
      button.getElement('background').setStrokeStyle(1, 0xffffff);
    })
    .on('button.out', function (button, groupName, index, pointer, event) {
      button.getElement('background').setStrokeStyle();
    });
  dialog.layout();

  return dialog;
};
export default CreateRequestModalBoard;
