import ObjectWrapper from 'src/utils/objectWrapper';
import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateDropDownList from './CreateDropDownList';
import { EndlessCanvas } from 'src/types/kanban_types';
import { EventBus, Events } from 'src/boot/eventBus';

const CreateHeader = function (scene: EndlessCanvas, board: ObjectWrapper) {
  const sizer = scene.rexUI.add
    .sizer({
      orientation: 'x',
    })
    .addBackground(scene.rexUI.add.roundRectangle(0, 0, 20, 20, 0, COLOR_DARK));
  const headerLabel = scene.rexUI.add.label({
    text: scene.add.text(0, 0, board.name),
  });

  EventBus.$on(Events.Change, (uuid, object) => {
    if (uuid === board.uuid) {
      //@ts-ignore
      headerLabel.setText(scene.store.getCurrentState.get(uuid).name);
    }
  });

  const dropDownButton = CreateDropDownList(scene, board);

  sizer
    .add(headerLabel, { proportion: 1, expand: true })
    .add(dropDownButton, { proportion: 0, expand: true });

  return sizer;
};
export default CreateHeader;
