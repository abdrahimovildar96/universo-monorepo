// import 'phaser';
// import UIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin';
import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateColumnPanelsBox from './CreateColumnPanelsBox';
import CreateHeader from './CreateHeader';
import AddResizeController from './AddResizeController';
import ObjectWrapper from 'src/utils/objectWrapper';
import { EndlessCanvas } from 'src/types/kanban_types';
import { EventBus, Events } from 'src/boot/eventBus';

const boardsBoxMap = new Map();

const CreateScrollablePanel = (
  scene: EndlessCanvas,
  board: ObjectWrapper,
  size: { w: number; h: number },
) => {
  const config = {
    adaptThumbSizeMode: true,
    width: size.w - 30,
    height: size.h - 30,
    //@ts-ignore
    background: scene.rexUI.add.roundRectangle({
      radius: 10,
      strokeColor: COLOR_DARK,
    }),
    panel: {
      child: CreateColumnPanelsBox(scene, board?.childrens[0], board.uuid),
      mask: {
        padding: 2,
        updateMode: 'everyTick',
      },
    },
    sliderX: {
      track: { width: 20, radius: 10, color: COLOR_DARK },
      thumb: { radius: 13, color: COLOR_LIGHT },
      hideUnscrollableSlider: true,
      input: 'click',
    },
    sliderY: {
      track: { width: 20, radius: 10, color: COLOR_DARK },
      thumb: { radius: 13, color: COLOR_LIGHT },
      hideUnscrollableSlider: true,
      input: 'click',
    },
    scrollerX: false,
    scrollerY: false,
    space: {
      left: 10,
      right: 10,
      top: 10,
      bottom: 10,
      sliderX: 10,
      sliderY: 10,
    },
    header: CreateHeader(scene, board),
  };

  // Создаем ScrollablePanel
  //@ts-ignore

  const scrollablePanel = scene.rexUI.add.scrollablePanel(config);

  // Устанавливаем возможность перетаскивания
  scrollablePanel.setDraggable('header');

  // Вызываем метод layout для обновления компоновки
  scrollablePanel.layout();

  // Добавляем контроллер для изменения размера
  AddResizeController(scrollablePanel);
  EventBus.$on(Events.Delete, (uuid, object) => {
    if (boardsBoxMap.has(uuid)) {
      boardsBoxMap.get(uuid).destroy();
    }
  });
  return scrollablePanel;
};

const CreateMoreScrollablePanels = (
  scene: EndlessCanvas,
  boards: ObjectWrapper[],
  size: { h: number; w: number },
) =>
  boards.map((board: ObjectWrapper) => {
    const scrollablePanel = CreateScrollablePanel(scene, board, size);
    boardsBoxMap.set(board.uuid, scrollablePanel);

    return scrollablePanel;
  });

export default CreateMoreScrollablePanels;
