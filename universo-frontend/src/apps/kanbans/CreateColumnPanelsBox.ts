import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateColumnPanel from './CreateColumnPanel';
import AddDragDropColumnPanelBehavior from './AddDragDropColumnPanelBehavior';
import ObjectWrapper from 'src/utils/objectWrapper';
import { PanelsBoxType, EndlessCanvas } from 'src/types/kanban_types';
import { EventBus, Events } from 'src/boot/eventBus';

const CreateColumnPanelsBox = (
  scene: EndlessCanvas,
  swimlane: ObjectWrapper,
  parentUuid: string,
) => {
  const config = {
    orientation: 'x',
    space: {
      left: 10,
      right: 10,
      top: 10,
      bottom: 10,
    },
  };
  //@ts-ignore

  const columnPanelsBox = scene.rexUI.add.sizer(config) as PanelsBoxType;
  // .addBackground(
  //     scene.rexUI.add.roundRectangle({
  //         strokeColor: COLOR_PRIMARY,
  //         strokeWidth: 3,
  //     }),
  //     'background'
  // )
  const panelsBoxMap = new Map();
  if (swimlane && swimlane.childrens && swimlane.childrens.length > 0) {
    swimlane.childrens.forEach((column: ObjectWrapper) => {
      const columnPanel = CreateColumnPanel(scene, column);
      panelsBoxMap.set(column.uuid, columnPanel);
      parentUuid = column.parentUuid;

      columnPanelsBox.add(columnPanel, { proportion: 0, expand: true });
    });
  }

  const box = new AddDragDropColumnPanelBehavior(columnPanelsBox);
  EventBus.$on(Events.Delete, (uuid, object) => {
    if (panelsBoxMap.has(uuid)) {
      //@ts-ignore
      box.panelsBox.remove(panelsBoxMap.get(uuid), true);
      box.panelsBox.layout();
    }
  });

  EventBus.$on(Events.Create, (parentUuid1, object) => {
    if (parentUuid === parentUuid1) {
      const columnPanel = CreateColumnPanel(scene, object);
      panelsBoxMap.set(object.uuid, columnPanel);

      box.addPanel(columnPanel);
      box.panelsBox.getTopmostSizer().layout();
    }
  });
  return box.panelsBox;
};

export default CreateColumnPanelsBox;
