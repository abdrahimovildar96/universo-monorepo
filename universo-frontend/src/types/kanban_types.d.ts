import UIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin';
import { Pinia } from 'pinia';
import { UIPluginExt } from 'src/types/kanban_types';

interface UIPluginExt extends UIPlugin {}

interface EndlessCanvas extends Phaser.Scene {
  rexUI: UIPluginExt;
  toolbarWidth?: number;
  mainMenuHeight?: number;
  store?: Pinia;
  router?: Router;
}

interface PanelsBoxType extends UIPlugin.Sizer {
  add(
    columnPanel: any,
    config: { proportion: number; expand: boolean },
  ): PanelsBoxType;
  getElement: (element: string) => PanelType[];
  remove: (panel: PanelType) => void;
  insertAtPosition: (
    x: number,
    y: number,
    panel: PanelType,
    config: { expand: boolean },
  ) => PanelsBoxType;
  getTopmostSizer: () => { layout: () => void };
  getParentSizer: () => PanelsBoxType;
}

type PanelType = {
  children: any;
  x: number;
  y: number;
  layout: () => PanelType;
  on: (
    event: string,
    callback: (pointer?: Phaser.Input.Pointer) => void,
  ) => PanelType; // Заменил Function на более конкретный тип
  setData: (data: Record<string, unknown>) => PanelType;
  getData: (key: string) => any; // Можно уточнить тип, если известно, какие данные могут быть получены
  moveFrom: (config: { x: number; y: number; speed: number }) => PanelType;
};

interface ItemsBoxType extends UIPlugin.Sizer {
  add(
    label: Label,
    config: { proportion: number; expand: boolean },
  ): ItemsBoxType;
  addBackground(item: RoundRectangle, color: string): ItemsBoxType;
  getElement: (name: string) => ItemType[];
  getParentSizer: () => ItemsBoxType;
  getChildIndex: (item: ItemType) => number;
  remove: (item: ItemType) => void;
  insert: (
    index: number,
    item: ItemType,
    options: { expand: boolean },
  ) => ItemsBoxType;
  getTopmostSizer: () => ItemsBoxType;
}

type CallbackType = (
  event: string,
  callback:
    | ((
        pointer?: Phaser.Input.Pointer,
        dragX?: number,
        dragY?: number,
        dropped?: boolean,
      ) => void)
    | ((pointer?: Phaser.Input.Pointer, dropZone: ItemType) => void),
) => ItemType;

interface ItemType extends UIPlugin.Sizer {
  text: any;
  on: CallbackType;
  setData: ((key: string, item: ItemTypeBox) => ItemType) &
    ((data: Record<string, any>) => ItemType);
  getData: (key: string) => any;
  x: number;
  y: number;
  moveFrom: (options: { x: number; y: number; speed: number }) => void;
}
