import en from './en';
import ru from './ru';

export default {
  'en-US': en,
  'en-UK': en,
  en: en,
  'en-GB': en,
  'en-CA': en,
  'en-AU': en,
  'en-NZ': en,
  'en-ZA': en,
  'en-IN': en,
  'en-SG': en,
  'en-MY': en,
  'en-PH': en,
  ru: ru,
  'ru-RU': ru,
  'ru-UA': ru,
  'ru-BY': ru,
  'ru-KZ': ru,
  'ru-TM': ru,
  'ru-UZ': ru,
  'ru-KG': ru,
  'ru-MD': ru,
};
