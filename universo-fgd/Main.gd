extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	var max_size := Vector2(1920, 1080)  # Максимальный размер окна
	OS.set_window_size(max_size)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
